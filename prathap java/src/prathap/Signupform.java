package prathap;

import java.awt.EventQueue;
import java.awt.Font;
import java.sql.*;
import javax.swing.*;



import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Signupform {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Signupform window = new Signupform();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	Connection connection = null;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JPasswordField passwordField_2;
	public Signupform() {
	
		connection = MysqlConnection.Dbconnecter();
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 850, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblSignupForm = new JLabel("signup form");
		lblSignupForm.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblSignupForm.setBounds(158, 13, 143, 40);
		frame.getContentPane().add(lblSignupForm);
		
		JLabel lblNewlabel = new JLabel("first name");
		lblNewlabel.setBounds(83, 87, 93, 16);
		frame.getContentPane().add(lblNewlabel);
		
		JLabel lblNewLabel = new JLabel("last name");
		lblNewLabel.setBounds(83, 116, 93, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("email address");
		lblNewLabel_1.setBounds(83, 168, 110, 16);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("password");
		lblNewLabel_2.setBounds(83, 215, 56, 16);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("conform password");
		lblNewLabel_3.setBounds(83, 261, 127, 16);
		frame.getContentPane().add(lblNewLabel_3);
		
		textField = new JTextField();
		textField.setBounds(223, 84, 116, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(223, 120, 116, 22);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(223, 165, 116, 22);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnNewButton = new JButton("Signup");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
		try {
				
					
					String queies = "insert into signup (firstname,lastname,emailaddress,password,conformpassword) values(?,?,?,?,?)";
					PreparedStatement pst = connection.prepareStatement(queies);
					
					
					
					pst.setString(1, textField.getText());
					pst.setString(2, textField_1.getText());
					pst.setString(3, textField_2.getText());
					pst.setString(4, passwordField.getText());
					pst.setString(5, passwordField_1.getText());
					
					 pst.execute();
					JOptionPane.showMessageDialog(null, "signup suceessfully");
					
					pst.close();
					}catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(171, 316, 97, 25);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblLoginForm = new JLabel("login form");
		lblLoginForm.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblLoginForm.setBounds(573, 18, 127, 31);
		frame.getContentPane().add(lblLoginForm);
		
		textField_5 = new JTextField();
		textField_5.setBounds(600, 84, 173, 22);
		frame.getContentPane().add(textField_5);
		textField_5.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("Login");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					String query = "select * from signup where emailaddress=? and password=?";
					PreparedStatement pst = connection.prepareStatement(query);
					
					pst.setString(1, textField_5.getText());
					pst.setString(2, passwordField_2.getText());
					
					ResultSet rs = pst.executeQuery();
					
					int count = 0;
					while (rs.next()) {
						count++;
						
					}
					if (count==1) {
						JOptionPane.showMessageDialog(null, "username and password are correct");
						frame.dispose();
						Book books = new Book();
						books.setVisible(true);
					} else if(count>1) {
						JOptionPane.showMessageDialog(null, "username and password are duplicate");
					}else {
						JOptionPane.showMessageDialog(null, "username and password are wrong.try again");
					}
					rs.close();pst.close();
				} catch (Exception e1) {
					// TODO: handle exception
				}
			}
		});
		btnNewButton_1.setBounds(573, 199, 97, 25);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel_4 = new JLabel("email id");
		lblNewLabel_4.setBounds(500, 87, 93, 16);
		frame.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("password");
		lblNewLabel_5.setBounds(500, 139, 56, 16);
		frame.getContentPane().add(lblNewLabel_5);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(223, 212, 116, 19);
		frame.getContentPane().add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(222, 258, 116, 19);
		frame.getContentPane().add(passwordField_1);
		
		passwordField_2 = new JPasswordField();
		passwordField_2.setBounds(600, 136, 179, 22);
		frame.getContentPane().add(passwordField_2);
	}
}
